package com.free4inno.knowledgems.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;

/**
 * Author HUYUZHU.
 * Date 2021/1/29 16:12.
 */
@Entity
@Table(name = "signup_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignupInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id; //主键

    @Column(name = "name")
    private String name; //真实姓名

    @Column(name = "telnumber")
    private String telnumber; //手机号码

    @Column(name = "mail")
    private String mail; //邮箱

    @Column(name = "reason", columnDefinition = "TEXT")
    private String reason; //申请理由

    @Column(name = "status")
    private Integer status; //申请状态（0为待审核，1为已通过，2为已拒绝，3为其他）

    @Column(name = "signup_time")
    private Timestamp signupTime; //申请时间

    @Column(name = "process_time")
    private Timestamp processTime; //处理时间

    @Column(name = "crop_id")
    private Integer cropId;

}
