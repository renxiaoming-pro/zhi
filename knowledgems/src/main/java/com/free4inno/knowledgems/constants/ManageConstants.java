package com.free4inno.knowledgems.constants;

/**
 * 作者：LHJ
 */
public class ManageConstants {

    public static final String USER_NUM = "userNum";
    public static final String GROUP_NUM = "groupNum";
    public static final String LABEL_NUM1 = "labelNum1";//一级标签
    public static final String LABEL_NUM2 = "labelNum2";//二级标签
    public static final String TEMPLATE_NUM = "templateNum";//模板

    //system manage table in database
    public static final String ES_ADDR = "es_addr";
    public static final String ES_PERIOD = "es_period";
    public static final String NFS_ADDR = "nfs_addr";
    public static final String MYSQL_ADDR = "mysql_addr";
    public static final String BOOK_LABEL = "book_label";
    public static final String APPKEY_PUBLIC = "appKey_public";
    public static final String DEFAULT_PASSWORD = "default_password";

    public static final String TIKA_ADDR = "tika_addr";
    public static final String TIKA_PERIOD = "tika_period";
    public static final String TIKA_RETRY = "tika_retry";
    public static final String NFS_TOTAL = "nfs_total";
    public static final String NFS_INCREMENT = "nfs_increment";

    public static final String KMS_VERSION = "kms_version";
    public static final String TIME_DELAY = "time_delay";

    public static final String QUERY = "query";
    public static final String RESULT = "result";
    //重建索引
    public static final String OK = "ok";//备份索引成功
    public static final String ERROR_REBUILD = "error when rebuild";//重建时错误，重建失败
    public static final String ABNORMAL = "error when backup";//备份索引异常，重建失败
    public static final String NO_PERMISSION = "user permission denied";//用户没有权限
    //删除索引
    public static final String ERROR_DELETE = "error when delete";
    //appkey (暂时写死)
    public static final String APPKEY = "oA7Is5L0CihjIB5w";
    //备份索引
    public static final String ERROR_BACKUP = "error when back up";

}

