package com.free4inno.knowledgems.constants;


public class TemplateConstants {

    public static final String TEMPLATE_INFO = "templateInfo";

    public static final String TEMPLATE_LIST = "templateList";
    public static final String MAX_PAGES = "maxPages";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String SEARCH = "search";
    public static final String SEARCHED_TEMPLATENUM = "searchedTemplateNum";

}
