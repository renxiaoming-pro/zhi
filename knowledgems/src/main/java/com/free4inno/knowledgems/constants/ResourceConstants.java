package com.free4inno.knowledgems.constants;

/**
 * 作者：LHJ
 */
public class ResourceConstants {

    public static final String DEFAULT_NAME = "未知";//默认book label id
    public static final String ATTACHMENT = "attachment";//附件

    public static final String OPEN_RESOURCE= "打开资源";
    public static final String DELETE_RESOURCE= "删除资源";
    public static final String NOTPULIC_RESOURCE = "当前资源为非公开资源";
    public static final String NOTPULIC_NOTLOGIN = "当前资源为非公开资源且未登录，跳转登录页面";
    public static final String MODIFY_RESOURCE_PERMISSION = "修改资源权限";
    public static final String GET_COMMENT = "获取资源评论";
    public static final String ADD_COMMENT = "新增评论";
    public static final String UPDATE_COMMENT = "编辑评论";
    public static final String DELETE_COMMENT = "删除评论";

    public static final String RESOURCE = "resource";
    public static final String USER = "user";
    public static final String BOOK_LABEL_NAME = "bookLabelName";

    public static final String COMMENT_LIST = "commentList";
    public static final String PAGE_TOTAL = "pageTotal";
    public static final String COMMENT_TOTAL = "commentTotal";
    public static final String RESOURCE_ID = "resourceId";
    public static final String USER_ID = "userId";
    public static final String USER_NAME = "userName";

}
