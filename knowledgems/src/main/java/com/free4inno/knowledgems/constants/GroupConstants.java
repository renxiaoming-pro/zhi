package com.free4inno.knowledgems.constants;

/**
 * 作者：LHJ
 */
public class  GroupConstants {
    public static final String ROLE_ID = "roleId"; //保存在session中的用户类型

    public static final String GROUP_LEADER = "未知群主"; //默认群主
    public static final String GROUP_NAME = "未知组名"; //默认群组名
    public static final String GROUP_INTRODUCTION = "群主是个很忙（lan）的人，所以没有写群组介绍~"; //默认群组介绍

    //写入成员信息
    public static final String GROUP_ID = "groupId";
    public static final String GROUP_INFO = "groupInfo";
    public static final String GROUP_MEMBERS = "GroupsMembers";
    public static final String USER_NAME = "userName";
    public static final String USER_PERMISSION = "userPermission";
    public static final String ROLE_IDNUM = "roleID";


    public static final String GROUP_LIST = "groupList";
    public static final String MAX_PAGES = "maxPages";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String SEARCH = "search";
    public static final String SEARCHED_GROUPNUM = "searchedGroupNum";

    public static final String MEMBER_LIST = "memberList";

}
