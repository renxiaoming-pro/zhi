package com.free4inno.knowledgems.constants;

/**
 * Author HUYUZHU.
 * Date 2020/9/27 11:33.
 */
public class UserConstants {
    public static final String USERNAME = "未知"; //用户真实姓名默认值
    public static final String ACCOUNT = "account"; //保存在session中的账户（手机号）
    public static final String ACCOUNT_NAME = "accountName"; //保存在session中的账户名(昵称)
    public static final String ACCOUNT_MAIL = "accountMail"; //保存在session中的用户邮箱
    public static final String USER_ID = "userId"; //保存在session中的用户id
    public static final String USER_NAME = "userName"; //保存在session中的用户真实姓名
    public static final String COMPLETE_URI = "completeUri"; //保存在session中的被拦截的uri

    public static final String USER_NUM = "userNum";
    public static final String USER_INFOS = "UserInfos";

    //新增用户
    public static final String CODE = "code";
    public static final String MSG = "msg";
    public static final String RESULT = "result";
    public static final String DEFAULT_PASSWORD = "defaultPassword";

    //用户角色
    public static final String AVERAGE_USER = "普通用户";
    public static final String CONTENT_ADMINISTRATOR = "内容管理员";
    public static final String USER_ADMINISTRATOR = "用户管理员";
    public static final String SYSTEM_ADMINISTRATOR = "系统管理员";
    public static final String INTERNAI_USER = "普通用户（内部用户）";
    public static final String VISITOR = "普通用户（访客）";

    //list
    public static final String USER_LIST = "userList";
    public static final String SEARCH_USERNUM = "searchedUserNum";
    public static final String MAXPAGES = "maxPages";
    public static final String CURRENT_PAGE = "currentPage";
    public static final String APPLY_LIST = "applyList";
    public static final String SEARCHED_APPLY_NUM = "searchedApplyNum";

    //count number
    public static final String MANAGER_NUM = "managerNum";
    public static final String APPLY_NUM = "applyNum";
    public static final String REFUSE_NUM = "refuseNum";


    public static final String NO_USERINFO = "Not found user info ";


}
