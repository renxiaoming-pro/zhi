package com.free4inno.knowledgems.openapi;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.free4inno.knowledgems.dao.*;
import com.free4inno.knowledgems.domain.*;
import com.free4inno.knowledgems.service.AppKeyService;
import com.free4inno.knowledgems.service.ResourceEsService;
import com.free4inno.knowledgems.service.ResourceService;
import com.free4inno.knowledgems.constants.UserConstants;
import com.free4inno.knowledgems.service.SearchService;
import com.free4inno.knowledgems.utils.LogUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.*;

/**
 * Author HUYUZHU.
 * Date 2021/10/24 16:58.
 * Open API
 * TODO 接口已做统一返回处理，返回的数据类型不可为String，以免与Spring机制产生BUG。如有需要返回String格式的data，请调用Result的静态方法封装到Result<String>中返回！
 */

@Slf4j
@RestController
@RequestMapping("openapi")
@Validated
public class OpenAPIController {

    @Autowired
    private GroupInfoDAO groupInfoDAO;

    @Autowired
    private LabelInfoDAO labelInfoDAO;

    @Autowired
    private ResourceService resourceService;

    @Autowired
    private ResourceDAO resourceDAO;

    @Autowired
    private AttachmentDAO attachmentDao;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private ResourceEsService resourceEsService;

    @Autowired
    private AppKeyService appKeyService;

    @Autowired
    private SearchService searchService;

    @GetMapping("demo")
    public Result<String> demoApi(
            @RequestParam(required = true, value = "testparam") @NotBlank String testparam) {
        return Result.success(testparam);
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper().disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    }

    @GetMapping("search")
    public Map<String, Object> search(
            @RequestParam(required = true, value = "appkey") String appKey,
            @RequestParam(required = true, value = "pageNum") @NotNull @Min(1) Integer pageNum,
            @RequestParam(required = true, value = "pageSize") @NotNull @Min(1) Integer pageSize,
            @RequestParam(required = true, value = "queryStr") String queryStr,
            @RequestParam(required = true, value = "groupNames") @NotBlank String groupNames,
            @RequestParam(required = true, value = "labelNames") @NotBlank String labelNames) {
        Map<String, Object> jsonObject = new HashMap<>();

        // ============================== 检验参数 ==============================
        List<String> searchGroups = new ArrayList<>();
        List<String> searchLabels = new ArrayList<>();
        try {
            // 群组，标签：以汉字JSONArray格式传入，首先校验并转换格式
            searchGroups = JSON.parseArray(groupNames, String.class);
            searchLabels = JSON.parseArray(labelNames, String.class);
        } catch (Exception e) {
            log.error(LogUtils.parse("openapi搜索", "群组/标签格式解析错误"));
            throw new OpenAPIException(ResultEnum.ARGS_ERROR);
        }
        ArrayList<String> groupIds = new ArrayList<>();
        ArrayList<String> labelIds = new ArrayList<>();
        for (String groupName : searchGroups) {
            // 群组：检查是否在标签列表中有对应id，不存在则报错
            GroupInfo groupInfo = groupInfoDAO.findByGroupName(groupName).orElse(new GroupInfo());
            if (groupInfo.getId() != null) {
                groupIds.add(groupInfo.getId().toString());
            } else {
                log.error(LogUtils.parse("openapi搜索", "群组名称解析错误，不存在：" + groupName));
                throw new OpenAPIException(ResultEnum.ARGS_ERROR);
            }
        }

        //openAPI搜索 http请求格式
        //http://localhost:8081/openapi/search?appkey=rff8m43c&pageNum=1&pageSize=10&queryStr=adobe&groupNames=[]&labelNames=["网络书签"]
        for (String labelName : searchLabels) {
            // 标签：检查是否在标签列表中有对应id，不存在则报错
            LabelInfo labelInfo = labelInfoDAO.findByLabelName(labelName).orElse(new LabelInfo());
            if (labelInfo.getId() != null && labelInfo.getUplevelId() != 0) {
                labelIds.add(labelInfo.getId().toString());
            } else {
                log.error(LogUtils.parse("openapi搜索", "标签名称解析错误，不存在：" + labelName));
                throw new OpenAPIException(ResultEnum.ARGS_ERROR);
            }
        }
        log.info(LogUtils.parse("openapi搜索", "验参通过"));

        // ============================== 业务处理 ==============================
        // 1. 根据appKey提取身份 -----------------------
        Map<String, Object> appKeyRes = appKeyService.checkAppKey(appKey);
        String userId=appKeyRes.get("userId").toString();
        Page<ResourceES> resourceESPage;
        if (appKeyRes.get("identity").equals("public")) {
            log.info(LogUtils.parse("openapi搜索", "根据appKey判断用户身份为public，调用公开搜索"));
            // 2.1. 调用 ResourceEsService 进行搜索 ----------- public
            resourceESPage = resourceEsService.searchResourceES(pageNum - 1, pageSize, queryStr, groupIds, labelIds, 1, false,"1");
        } else if (appKeyRes.get("identity").equals("user")) {
            log.info(LogUtils.parse("openapi搜索", "根据appKey判断用户身份为user，调用非公开搜索"));
            // 2.2. 调用 ResourceEsService 进行搜索 ----------- login
            resourceESPage = resourceEsService.searchResourceES(pageNum - 1, pageSize, queryStr, groupIds, labelIds, 1, true, userId);

        } else {
            throw new OpenAPIException(ResultEnum.ACCESS_ERROR);
        }

        // 3. 构建返回 ---------------------------------
        try {
            // 结果资源总数 & 总页数
            long totalResources = resourceESPage.getTotalElements();
            long totalPages;
            if (totalResources % pageSize == 0) {
                totalPages = totalResources / pageSize;
            } else {
                totalPages = totalResources / pageSize + 1;
            }

            // 遍历搜索结果获取搜索结果列表中展示的标题、正文、作者名、群组名、标签名等信息
            List<ResourceES> resourceESList = resourceESPage.getContent();

            resourceESList = searchService.buildResourceES(resourceESList);

            // 查询信息
            jsonObject.put("queryStr", queryStr);
            jsonObject.put("groupNames", searchGroups);
            jsonObject.put("labelNames", searchLabels);
            // 分页信息
            jsonObject.put("pageNum", pageNum);
            jsonObject.put("pageSize", pageSize);
            jsonObject.put("totalPages", totalPages);
            jsonObject.put("totalResources", totalResources);
            // 资源信息
            jsonObject.put("resource", resourceESList);

        } catch (Exception e) {
            log.error(LogUtils.parse("openapi搜索", "解析搜索结果错误"));
            throw new OpenAPIException(ResultEnum.WORK_ERROR);
        }

        log.info(LogUtils.parse("openapi搜索", "成功"));
        return jsonObject;
    }

    @GetMapping("detail")
    public Map<String, Object> detail(
            @RequestParam(required = true, value = "appkey") String appKey,
            @RequestParam(required = true, value = "id") @NotNull @Min(1) Integer id) {
        Map<String, Object> jsonObject = new HashMap<>();
        // 1. ========== 获取资源 ==========
        Resource resource = new Resource();
        try {
            resource = resourceDAO.findAllById(id);
            log.info(LogUtils.parse("openapi获取资源内容", "获取资源，id：" + id));
        } catch (Exception e) {
            log.error(LogUtils.parse("openapi获取资源内容", "未找到资源，id：" + id));
            throw new OpenAPIException(ResultEnum.WORK_ERROR);
        }

        // 2. ========== 根据appKey提取身份 ==========
        Map<String, Object> appKeyRes = appKeyService.checkAppKey(appKey);
        if (appKeyRes.get("identity").equals("public")) {
            log.info(LogUtils.parse("openapi获取资源内容", "appKey权限为public"));
            // 2.1. 判断是否为公开资源 ----------- public
            if (resource.getPermissionId() == 0) {
                /* 非公开资源 */
                log.warn(LogUtils.parse("openapi获取资源内容", "非公开资源，拒绝访问！"));
                throw new OpenAPIException(ResultEnum.FORBIDDEN);
            } else {
                /* 公开资源 */
                log.info(LogUtils.parse("openapi获取资源内容", "公开资源，允许访问！"));
            }
        } else if (appKeyRes.get("identity").equals("user")) {
            // 2.2. 判断登录用户是否有权限打开 ----------- login
            User appKeyUser = (User) appKeyRes.get("user");
            log.info(LogUtils.parse("openapi获取资源内容", "appKey权限为user，userId：" + appKeyUser.getId()));
            Boolean isInGroup = resourceService.isInResourceGroup(resource, appKeyUser.getId());
            if (isInGroup) {
                /* 用户有权打开 */
                log.info(LogUtils.parse("openapi获取资源内容", "该用户有权打开，允许访问！"));
            } else {
                /* 用户无权打开 */
                log.warn(LogUtils.parse("openapi获取资源内容", "该用户无权打开，拒绝访问！"));
                throw new OpenAPIException(ResultEnum.FORBIDDEN);
            }
        } else {
            throw new OpenAPIException(ResultEnum.ACCESS_ERROR);
        }

        // 3. ========== 构建返回 ==========
        try {
            //取用户名
            User user = new User();
            user.setRealName(UserConstants.USERNAME);
            user = userDAO.findById(resource.getUserId()).orElse(user);
            //取群组名
            resource.setGroupName(resourceService.getGroupName(resource));
            //取标签名
            resource.setLabelName(resourceService.getLabelName(resource));
            //取资源的附件
            String attachment = resourceService.getAttachmentJson(id);

            jsonObject.put("attachment", attachment);
            jsonObject.put("resource", resource);
            jsonObject.put("user", user.getRealName());

        } catch (Exception e) {
            log.error(LogUtils.parse("openapi获取资源内容", "解析资源错误"));
            throw new OpenAPIException(ResultEnum.WORK_ERROR);
        }
        log.info(LogUtils.parse("openapi获取资源内容", "成功"));
        return jsonObject;
    }

    @GetMapping("new")
    public Map<String, Object> newResource(
            @RequestParam(required = true, value = "appkey") String appKey,
            @RequestParam(required = true, value = "label") String label,
            @RequestParam(required = true, value = "group") String group,
            @RequestParam(required = true, value = "title") @NotNull String title,
            @RequestParam(required = true, value = "text") @NotNull String text) {
        Map<String, Object> jsonObject = new HashMap<>();

        // 1. ========== 根据appKey提取身份 ==========
        Integer userId = -1;
        Map<String, Object> appKeyRes = appKeyService.checkAppKey(appKey);
        if (appKeyRes.get("identity").equals("user")) {
            /* 用户有权新增 */
            userId = ((User) appKeyRes.get("user")).getId();
            log.info(LogUtils.parse("openapi创建资源", "身份user有权新增，userId：" + userId));
        } else {
            /* 用户无权新增 */
            log.error(LogUtils.parse("openapi创建资源", "身份public无权新增，appKey：" + appKey));
            throw new OpenAPIException(ResultEnum.ACCESS_ERROR);
        }

        try {
            // 2. ========== 执行新增 ==========
            Resource resource = new Resource();
            resource.setUserId(userId);
            resource.setCreateTime(new Timestamp(new Date().getTime()));
            resource.setEditTime(new Timestamp(new Date().getTime()));
            resource.setTitle(title);
            resource.setText(text);
            resource.setPermissionId(0);    // 默认不公开资源
            resource.setSuperior(0);        // 默认不推荐资源
            resource.setGroupId(group);
            resource.setLabelId(label);
            resourceDAO.saveAndFlush(resource);
            resourceEsService.addResourceES(resource.getId());

            jsonObject.put("msg", "success");
            jsonObject.put("resourceId", resource.getId());
        } catch (Exception e) {
            log.error(LogUtils.parse("openapi创建资源", "资源创建过程中错误"));
            throw new OpenAPIException(ResultEnum.WORK_ERROR);
        }
        log.info(LogUtils.parse("openapi创建资源", "成功"));
        return jsonObject;
    }

    // 留此接口进行邮件新增的处理逻辑，与上面的new会有所不同，目前仍在开发中
    @GetMapping("newEmail")
    public Map<String, Object> newEmail(
            @RequestParam(required = true, value = "appkey") String appKey,
            @RequestParam(required = true, value = "title") @NotNull String title,
            @RequestParam(required = true, value = "text") @NotNull String text,
            @RequestParam("attachment") String attachmentJson) {
        Map<String, Object> jsonObject = new HashMap<>();

        // 1. ========== 根据appKey提取身份 ==========
        Integer userId = -1;
        Map<String, Object> appKeyRes = appKeyService.checkAppKey(appKey);
        if (appKeyRes.get("identity").equals("user")) {
            /* 用户有权新增 */
            userId = ((User) appKeyRes.get("user")).getId();
            log.info(LogUtils.parse("openapi创建资源", "身份user有权新增，userId：" + userId));
        } else {
            /* 用户无权新增 */
            log.error(LogUtils.parse("openapi创建资源", "身份public无权新增，appKey：" + appKey));
            throw new OpenAPIException(ResultEnum.ACCESS_ERROR);
        }

        try {
            // 2. ========== 执行新增 ==========
            Resource resource = new Resource();
            resource.setUserId(userId);
            resource.setCreateTime(new Timestamp(new Date().getTime()));
            resource.setEditTime(new Timestamp(new Date().getTime()));
            resource.setTitle(title);
            resource.setText(text);
            resource.setPermissionId(0);    // 默认不公开资源
            resource.setSuperior(0);        // 默认不推荐资源
            resourceDAO.saveAndFlush(resource);
            resourceEsService.addResourceES(resource.getId());

            //保存附件信息
            JSONArray attachmentJsonArray = JSON.parseArray(attachmentJson);
            System.out.println(attachmentJsonArray);
            for (int i = 0; i < attachmentJsonArray.size(); i++) {
                JSONObject attachmentJsonObject = attachmentJsonArray.getJSONObject(i);
                Attachment attachment = new Attachment();
                attachment.setResourceId(resource.getId());
                attachment.setCreateTime(new Timestamp(new Date().getTime()));
                attachment.setName(attachmentJsonObject.getString("name"));
                attachment.setUrl(attachmentJsonObject.getString("url"));
                attachment.setStatus(Attachment.AttachmentEnum.READY);
                attachmentDao.saveAndFlush(attachment);
            }
            jsonObject.put("msg", "SUCCESS");
            jsonObject.put("resourceId", resource.getId());
            log.info(LogUtils.parse("openapi创建Email", "成功"));
            return jsonObject;
        } catch (Exception e) {
            log.error(LogUtils.parse("openapi创建Email", "资源创建过程中错误"));
            throw new OpenAPIException(ResultEnum.WORK_ERROR);
        }
    }
}
