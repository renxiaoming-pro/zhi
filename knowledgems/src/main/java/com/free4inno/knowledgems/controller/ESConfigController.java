package com.free4inno.knowledgems.controller;

import com.free4inno.knowledgems.dao.*;
import com.free4inno.knowledgems.domain.*;
import com.free4inno.knowledgems.constants.UserConstants;
import com.free4inno.knowledgems.utils.WriteEsResourceHttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpSession;


@Controller
@Slf4j
@RequestMapping("/esconfig")
public class ESConfigController {

    @Autowired
    private AttachmentDAO attachmentDao;

    @Autowired
    private ResourceESDAO resourceESDao;
    @Autowired
    private ResourceDAO resourceDao;
    @Autowired
    private WriteEsResourceHttpUtils writeEsResourceHttpUtils;

    @Value("${spring.data.elasticsearch.cluster-nodes}")
    private String esNodes;

    @Value("${attatchment.download.url}")
    private String downloadUrl;

    @Value("${es.index.resource.name}")
    private String indexName;

    @RequestMapping("/fresh")
    public String fresh_es_attach(HttpSession session) {
        log.info(this.getClass().getName() + "----" + "刷新ES附件正文" + "----" + session.getAttribute(UserConstants.USER_ID));

        for (int i = 0; i < 20000; i++) {
            log.info(this.getClass().getName() + "----" + "正在刷新第" + i + "号文档----" + session.getAttribute(UserConstants.USER_ID));
            Resource resource = resourceDao.findResourceById(i);
            if(resource==null){continue;}
            ResourceES resource_es = new ResourceES();
            writeEsResourceHttpUtils.writeResourceToESHTTP(resource, resource_es);
            log.info(this.getClass().getName() + "----" + i + "号文档更新成功----" + session.getAttribute(UserConstants.USER_ID));
        }

        return "success";
    }
}
