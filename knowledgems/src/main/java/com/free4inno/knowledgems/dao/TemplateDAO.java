package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.GroupInfo;
import com.free4inno.knowledgems.domain.Template;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Author: ZhaoHaotian.
 * Date: 2021/4/27.
 */
@Repository
public interface TemplateDAO extends JpaRepository<Template, Integer> {
    Optional<Template> findById(Integer id);

    Page<Template> findAll(Pageable pageable);

}
