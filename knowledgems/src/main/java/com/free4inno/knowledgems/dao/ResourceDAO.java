package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * ResourceDao.
 */
@Repository
public interface ResourceDAO extends JpaRepository<Resource, Integer> {
    Optional<Resource> findById(Integer id);

    Resource findAllById(Integer id);

    List<Resource> findByUserId(Integer userId);

    Resource findResourceById(Integer id);

    @Transactional
    void deleteById(Integer id);

    Page<Resource> findAllByUserId(Pageable pageable, Integer userId);

    List<Resource> findByGroupIdContaining(String groupId);

    List<Resource> findByLabelIdContaining(String labelId);

    Page<Resource> findByIdIn(Pageable pageable, List<Integer> ids);

}
