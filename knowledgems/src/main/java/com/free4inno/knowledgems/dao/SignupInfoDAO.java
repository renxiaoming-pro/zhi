package com.free4inno.knowledgems.dao;

import com.free4inno.knowledgems.domain.SignupInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Author HUYUZHU.
 * Date 2021/1/29 16:25.
 */
@Repository
public interface SignupInfoDAO extends JpaRepository<SignupInfo, Integer> {
    int countAllByStatus(Integer status);

    Page<SignupInfo> findAllByStatus(Integer status, Pageable pageable);

    Page<SignupInfo> findAllByStatusAndNameLikeOrStatusAndTelnumberLikeOrStatusAndMailLike(
            Integer status1, String name, Integer status2, String telnumber, Integer status3, String mail, Pageable pageable);

    Page<SignupInfo> findAllByStatusOrStatus(Integer status1,Integer status2, Pageable pageable);

    Page<SignupInfo> findAllByStatusAndNameLikeOrStatusAndNameLikeOrStatusAndTelnumberLikeOrStatusAndTelnumberLikeOrStatusAndMailLikeOrStatusAndMailLikeOrStatusAndReasonLikeOrStatusAndReasonLike(
            Integer status1, String name1,Integer status2, String name2, Integer status3, String telnumber1, Integer status4, String telnumber2, Integer status5, String mail1, Integer status6, String mail2,Integer status7, String reason1, Integer status8, String reason2, Pageable pageable);
}
