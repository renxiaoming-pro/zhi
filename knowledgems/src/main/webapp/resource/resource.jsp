<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/9/4
  Time: 15:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>${resource.title}-知了[团队知识管理应用]</title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
</head>

<body>
<div class="re-main">
    <c:import url="../template/_navbar.jsp"/>
    <button class="box_top hover_box" style="z-index: 2" onclick="goToBottom()">
        <img src="../assets/images/Down.png" height="20" width="20" />
    </button>
    <div class="re-box-6 mt-85">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="re-box re-box-decorated resource-post">
                        <div class="resource-post-box">
                            <%-------------------- 资源标题 --------------------%>
                            <h1 class="h3 resource-post-title">${resource.title}</h1>
                            <%-------------------- 资源标签 --------------------%>
                            <c:if test="${!resource.labelName.equals(\"该资源无标签\")}">
                                <ul class="resource-post-info">
                                    <li class="font-text-darkgray">标签:&nbsp;</li>
                                    <c:forEach var="label" items="${resource.labelName.split(',|，')}" varStatus="s">
                                        <c:forEach var="labelId" items="${resource.labelId.split(',|，')}"
                                                   varStatus="gi">
                                            <c:if test="${s.index==gi.index}">
                                                <li><a class="font-text font-text-darkgray"
                                                       href="../labels/labels?id=${labelId.trim()}">${label.trim()}</a>
                                                </li>
                                            </c:if>
                                        </c:forEach>
                                    </c:forEach>
                                </ul>
                            </c:if>
                            <%-------------------- 资源群组 --------------------%>

                            <c:if test="${!resource.groupName.equals(\"该资源无群组\")}">
                                <ul class="resource-post-info col-md-4">
                                    <c:forEach var="groupName" items="${resource.groupName.split(',|，')}"
                                               varStatus="gn">
                                        <c:forEach var="groupId" items="${resource.groupId.split(',|，')}"
                                                   varStatus="gi">
                                            <c:choose>
                                                <c:when test="${gi.index==gn.index}">
                                                    <c:if test="${groupId.trim() != 0 && groupId.trim() != 1}">
                                                        <li class="font-text-darkgray">群组:&nbsp;</li>
                                                        <li><a class="font-text-darkgray"
                                                               href="../groupinfo?groupid=${groupId.trim()}">${groupName.trim()}</a>
                                                        </li>
                                                    </c:if>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                    </c:forEach>
                                </ul>
                            </c:if>
                            <%-------------------- 资源作者 --------------------%>
                            <ul class="resource-post-info">
                                <li class="font-text-darkgray">作者:&nbsp;</li>
                                <li><a href="/myresource?userId=${resource.userId}"
                                       class="font-text-darkgray">${user.realName}</a></li>
                            </ul>
                            <%-------------------- 资源时间 --------------------%>
                            <ul class="resource-post-info">
                                <li class="font-text-darkgray">时间:&nbsp;</li>
                                <li class="font-text-darkgray"><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
                                                                               value="${resource.createTime}"/></li>
                            </ul>
                            <%-------------------- 资源操作 --------------------%>
                            <div id="operation">
                                <c:if test="${sessionScope.userId==user.id or sessionScope.roleId==1 or sessionScope.roleId==3}">
                                    <button class="re-btn re-btn-unselected re-label-child" onclick="edit()">编辑
                                    </button>
                                    <button class="re-btn re-btn-unselected re-label-child" onclick="deleteResource()">
                                        删除
                                    </button>
                                    <button id="permission" class="re-btn re-btn-unselected re-label-child"
                                            onclick="changePermission()"></button>
                                    <c:if test="${sessionScope.roleId==1 or sessionScope.roleId==3}">
                                        <button id="recommend" class="re-btn re-btn-unselected re-label-child"
                                                onclick="showRecommendOrder()"></button>
                                        <button id="notice" class="re-btn re-btn-unselected re-label-child"
                                                onclick="showNoticeOrder()"></button>
                                    </c:if>
                                </c:if>
                                <c:if test="${not empty sessionScope.userId}">
                                    <%-------------------- 导出PDF按钮 --------------------%>
                                    <button class="re-btn re-btn-unselected re-label-child"
                                            id="topdf"
                                            onclick="window.location.href='../topdf?id=${resource.id}'">导出PDF
                                    </button>
                                </c:if>
                            </div>
                        </div>
                        <%-------------------- 资源正文 --------------------%>
                        <div class="resource-post-box resource-post-article">
                            ${resource.text}
                        </div>
                        <%-------------------- 资源附件 --------------------%>
                        <c:if test="${(attachment ne '[]')&&(not empty attachment)}">
                            <div class="resource-post-box resource-post-attach">
                                <div class="font-text-darkgray">附件</div>
                                <div id="file-list"></div>
                            </div>
                        </c:if>
                    </div>
                    <%-------------------- 资源评论 --------------------%>
                    <div id="comment" class="resource-post" onblur=""></div>
                </div>
            </div>
        </div>
    </div>
    <button class="box_bottom hover_box" onclick="goToTop()">
        <img src="../assets/images/Up.png" height="20" width="20" />
    </button>
    <!-- START: Footer -->
    <c:import url="../template/_footer.jsp"/>
    <!-- END: Footer -->
</div>

<div class="modal fade" id="addRecommendOrder" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">设置为推荐资源</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>资源推荐排序</label>
                    <input type="text" class="form-control front-no-box-shadow" name="applicationDes"
                           id="recommendOrder" placeholder="请填写一个整数"
                           oninput="value=value.replace(/[^\-?\d]/g,'')"/>
                </div>
            </div>
            <div class="modal-footer">
                <%--                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>关闭</button>--%>
                <button type="button" id="btn_submit" class="btn btn-primary" data-dismiss="modal"
                        onclick="recommendResource()"><span class="glyphicon glyphicon-floppy-disk"
                                                            aria-hidden="true"></span><a>确定</a></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="addNoticeOrder" tabindex="-1" role="dialog" aria-labelledby="addMemberLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">设置为公告资源</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>资源推荐排序</label>
                    <input type="text" class="form-control front-no-box-shadow" name="applicationDes" id="noticeOrder"
                           placeholder="请填写一个整数" oninput="value=value.replace(/[^\-?\d]/g,'')"/>
                </div>
            </div>
            <div class="modal-footer">
                <%--                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span>关闭</button>--%>
                <button type="button" id="btn_submit" class="btn btn-primary" data-dismiss="modal"
                        onclick="noticeResource()"><span class="glyphicon glyphicon-floppy-disk"
                                                         aria-hidden="true"></span><a>确定</a></button>
            </div>
        </div>
    </div>
</div>

</body>
<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>

<script language="JavaScript">
    function goToTop() {
        window.scrollTo(0, 0);
    }
    function goToBottom() {
        console.log(111)
        window.scrollTo(0, document.documentElement.scrollHeight-document.documentElement.clientHeight);
    }

    userId = ${user_id};

    let _commentTotal = 0;
    let _thisPageTotal = 0;

    var id = ${resource.id};
    var roleId = '${sessionScope.roleId}';
    let firstGetComment = true; // 用于屏蔽首次加载评论时，屏幕滚动到评论起始

    function edit() {
        window.location.href = "<%=request.getContextPath()%>/crudres/edit?id=" + id;
    }

    $(document).ready(function () {
        getComment(1);
    })

    // 根据permissionId设置按钮文字
    $(document).ready(function () {
        // 公开操作
        if (${resource.permissionId==0}) {
            $("#permission").html("设置为公开");
        } else {
            $("#permission").html("设置为不公开");
        }
        // 推荐操作
        if (${resource.recommendType==1}) {
            $("#recommend").html("资源推荐排序为 ${resource.recommendOrder}");
            //设置资源推荐排序初始值
            document.getElementById("recommendOrder").setAttribute("value", ${resource.recommendOrder})
        } else {
            $("#recommend").html("设置为推荐");
        }
        // 公告操作
        if (${resource.noticeType==2}) {
            $("#notice").html("资源公告排序为 ${resource.noticeOrder}");
            //设置资源公告排序初始值
            document.getElementById("noticeOrder").setAttribute("value", ${resource.noticeOrder})
        } else {
            $("#notice").html("设置为公告");
        }
    })

    //设置为推荐资源按钮跳转
    function showRecommendOrder() {
        if (roleId == 1 || roleId == 3) {
            $('#addRecommendOrder').modal();
        } else {
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '您没有此权限'});
        }
    }

    //设置为公告资源按钮跳转
    function showNoticeOrder() {
        if (roleId == 1 || roleId == 3) {
            $('#addNoticeOrder').modal();
        } else {
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '您没有此权限'});
        }
    }

    //设置资源为推荐（ 弹窗 确定按钮调用）
    function recommendResource() {
        var recommendOrder = parseInt(document.getElementById("recommendOrder").value)
        if (isNaN(recommendOrder) || typeof recommendOrder != "number") {
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '请填写一个正/负整数'});
            return false
        } else {
            loading("show");
            console.log(recommendOrder)
            $.post("../specresource/crudSpec", {
                type: 1,
                resourceId: ${resource.id},
                order: recommendOrder
            }, function (data) {
                if (data.msg == "SUCCESS") {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '资源设置推荐成功'});
                    $("#recommend").html("资源推荐排序为 " + data.order);
                    //设置资源推荐排序初始值
                    document.getElementById("recommendOrder").setAttribute("value", data.order);
                    loading("reset");
                } else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '出问题了，重新试试！'});
                    loading("reset");
                }
            })

        }
    }

    //设置资源为公告（ 弹窗 确定按钮调用）
    function noticeResource() {
        var noticeOrder = parseInt(document.getElementById("noticeOrder").value)
        if (isNaN(noticeOrder) || typeof noticeOrder != "number") {
            $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '请填写一个正/负整数'});
            return false
        } else {
            loading("show");
            console.log(noticeOrder)
            $.post("../specresource/crudSpec", {
                type: 2,
                resourceId: ${resource.id},
                order: noticeOrder
            }, function (data) {
                if (data.msg == "SUCCESS") {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '资源设置公告成功'});
                    $("#notice").html("资源公告排序为 " + data.order);
                    //设置资源推荐排序初始值
                    document.getElementById("noticeOrder").setAttribute("value", data.order);
                    loading("reset");
                } else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '出问题了，重新试试！'});
                    loading("reset");
                }

            })
        }
    }



    //显示附件
    var attachment = ${attachment};
    $(document).ready(function () {
        var htmlStr = $("#file-list").html();
        var flag = 0
        for (var i = 0; i < attachment.length; i++) {
            // 支持“新文件下载接口”的同时，需要兼容旧“nfs下载”！
            var url = attachment[i].url;
            if (url.indexOf("/util/downloadAttachment") != -1) {
                if (attachment[i].status == "SUCCESS") {
                    htmlStr += "<div id=\"atta" + attachment[i].id + "\" style=\"color: darkgray\" class=\"mnt-7 font-text\"><a href=\"" + attachment[i].url + "&name=" + attachment[i].name + "\">" + attachment[i].name + "</a>&nbsp&nbsp已解析</div>";
                } else if (attachment[i].status == "READY") {
                    htmlStr += "<div id=\"atta" + attachment[i].id + "\" style=\"color: darkgray\" class=\"mnt-7 font-text\"><a href=\"" + attachment[i].url + "&name=" + attachment[i].name + "\">" + attachment[i].name + "</a>&nbsp&nbsp正在解析中</div>";
                } else if (attachment[i].status == "UNSUPPORTED") {
                    htmlStr += "<div id=\"atta" + attachment[i].id + "\" style=\"color: darkgray\" class=\"mnt-7 font-text\"><a href=\"" + attachment[i].url + "&name=" + attachment[i].name + "\">" + attachment[i].name + "</a>&nbsp&nbsp格式受限";
                    if (flag === 0) {
                        htmlStr += '<img src="../assets/images/alert.png" class="mytooltip" title="附件解析目前仅支持pdf、txt、doc、docx、ppt、pptx、xls、xlsx 等8种类型的文件" style="width: 15px;cursor:pointer;margin-left: 5px;margin-bottom: 3px"></div>'
                        flag = 1
                    } else {
                        htmlStr += '</div>'
                    }

                } else {
                    htmlStr += "<div id=\"atta" + attachment[i].id + "\" style=\"color: darkgray\" class=\"mnt-7 font-text\"><a href=\"" + attachment[i].url + "&name=" + attachment[i].name + "\">" + attachment[i].name + "</a>&nbsp&nbsp解析错误</div>";
                }
            } else {
                if (attachment[i].status == "SUCCESS") {
                    htmlStr += "<div id=\"atta" + attachment[i].id + "\" style=\"color: darkgray\" class=\"mnt-7 font-text\"><a href=\"" + attachment[i].url + "?flag=download&&attname=" + attachment[i].name + "\">" + attachment[i].name + "</a>&nbsp&nbsp已解析</div>";
                } else if (attachment[i].status == "READY") {
                    htmlStr += "<div id=\"atta" + attachment[i].id + "\" style=\"color: darkgray\" class=\"mnt-7 font-text\"><a href=\"" + attachment[i].url + "?flag=download&&attname=" + attachment[i].name + "\">" + attachment[i].name + "</a>&nbsp&nbsp正在解析中</div>";
                } else if (attachment[i].status == "UNSUPPORTED") {
                    htmlStr += "<div id=\"atta" + attachment[i].id + "\" style=\"color: darkgray\" class=\"mnt-7 font-text\"><a href=\"" + attachment[i].url + "?flag=download&&attname=" + attachment[i].name + "\">" + attachment[i].name + "</a>&nbsp&nbsp格式受限";
                    if (flag === 0) {
                        htmlStr += '<img src="../assets/images/alert.png" class="mytooltip" title="附件解析目前仅支持pdf、txt、doc、docx、ppt、pptx、xls、xlsx 等8种类型的文件" style="width: 15px;cursor:pointer;margin-left: 5px;margin-bottom: 3px"></div>'
                        flag = 1
                    } else {
                        htmlStr += '</div>'
                    }
                } else {
                    htmlStr += "<div id=\"atta" + attachment[i].id + "\" style=\"color: darkgray\" class=\"mnt-7 font-text\"><a href=\"" + attachment[i].url + "?flag=download&&attname=" + attachment[i].name + "\">" + attachment[i].name + "</a>&nbsp&nbsp解析错误</div>";
                }
            }
        }
        $("#file-list").html(htmlStr);
    })

    // 显示提示
    $(function () {
        var x = 10;
        var y = 20;
        var newtitle = '';
        var mytitleId = '';
        $('img.mytooltip').mouseover(function (e) {
            newtitle = this.title;
            this.title = '';
            $('body').append('<div id="mytitle" >' + newtitle + '</div>');
            $('#mytitle').css({
                'left': (e.pageX + x + 'px'),
                'top': (e.pageY + y - 80 + 'px')
            }).show();
        }).mouseout(function () {
            this.title = newtitle;
            $('#mytitle').remove();
        }).mousemove(function (e) {
            mytitleId = this.id;
            if (mytitleId == "pdl" || mytitleId == "ptv") {
                $('#mytitle').css({
                    'left': (e.pageX + x + 10 + 'px'),
                    // 'top': (e.pageY + y - 60 + 'px')
                    'top': (e.pageY + y - 120 + 'px')
                }).show();
            } else {
                $('#mytitle').css({
                    'left': (e.pageX + x + 10 + 'px'),
                    'top': (e.pageY + y - 60 + 'px')
                    // 'top': (e.pageY + y - 110 + 'px')
                }).show();
            }

        })
    });


    //设置资源为公开/不公开
    function changePermission() {
        $.ajax({
            url: "/resource/changePermission",
            type: "post",
            data: {
                id:${resource.id},
            },
            dataType: "text",
            success: function (data) {
                if (data == 'permissionId=0') {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '资源已设为不公开'});
                    $("#permission").html("设置为公开");
                } else {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '资源已设为公开'});
                    $("#permission").html("设置为不公开");
                }
            }
        });
    }


    //删除资源
    function deleteResource() {
        $.tipModal('confirm', 'warning', '确认删除资源吗?', function (result) {
            if (result == true) {
                for (var i = 0; i < attachment.length; i++) {
                    var attachmentId = attachment[i].id;
                    var saveUrl = attachment[i].url;
                    $.post("${pageContext.request.contextPath}/util/deleteAttachment", {
                        attachmentId: attachmentId,
                        saveUrl: saveUrl
                    });
                }
                window.location.href = "<%=request.getContextPath()%>/resource/deleteResource?id=" +${resource.id};
            }
        });
    }

    function getComment(page) {
        $("#page-data").html("");//清除页码，必要！！
        $.ajax({
            type: "get",
            url: "/resource/getComment",
            data: {
                resourceId: ${resource.id},
                page: page - 1
            },
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                $("#comment").html(data);
                let pageTotal = $('#page-total').html();
                if (pageTotal > 1) {
                    $("#page-data").html(getDivPageNumHtml(page, pageTotal, "getComment") + "<br /><br /><br /><br />");
                }
                if (firstGetComment) {
                    // 首次加载不滚动到评论起始位置
                    firstGetComment = false;
                } else {
                    // 滚动到评论起始位置，100ms
                    $('html,body').animate({
                        scrollTop: $('#comment').offset().top - 100
                    }, 100);
                }
                curPage = page;
            }
        });
    }

    /*
    function getLastComment(page) {
        $("#page-data").html("");//清除页码，必要！！
        $.ajax({
            type: "get",
            url: "/resource/getComment",
            data: {
                resourceId: ${resource.id},
                page: page - 1
            },
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                $("#comment").html(data);
                let pageTotal = $('#page-total').html();
                if (pageTotal > 1) {
                    $("#page-data").html(getDivPageNumHtml(page, pageTotal, "getComment") + "<br /><br /><br /><br />");
                }
                let count = 1;
                $("#comment-box > div").each(function () {
                    let id = $(this).attr('id');
                    if (id != 'comment-title') {
                        if (count == _thisPageTotal) {
                            // 滚动到评论起始位置，100ms
                            $('html,body').animate({
                                scrollTop: $("#" + id).offset().top - 80
                            }, 100);
                            // 展开它
                            doFold(1, id);
                        }
                        count++;
                    }
                });
            }
        });
    }
    */

    function submitComment() {
        if (${sessionScope.login_flag==1}) {
            var content = $("#content").val();
            if (content.replace(/\ +/g, "").replace(/[\r\n]/g, "").trim() == "") {
                $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '评论为空！'});
                return false;
            }
            var btn = $("#submitCommentButton");
            btn.attr("disabled", true);
            $.ajax({
                url: "/resource/newComment",
                type: "post",
                async: false,
                dataType: "text",
                data: {
                    content: content,
                    resourceId:${resource.id}
                },
                success: function (data) {
                    if (data.id == -1) {
                        $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '评论失败'});
                    } else {
                        //$("#content").val("");
                        $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '评论成功'});
                        // 添加评论
                        //addComment(data)
                        getComment(1);
                    }
                },
            });
        } else {
            window.location.href = "<%=request.getContextPath()%>/login";
        }
    }

    function submitRichComment() {
        button_text = document.getElementById('submitCommentButton').innerHTML;
        document.getElementById('submitCommentButton').innerHTML = '发布评论中...';

        if (${sessionScope.login_flag==1}) {
            // 获取带标签的content
            var content = tinymce.get('editor').getContent();
            // 获取不带标签的text
            var text = content.replace(/<(style|script|iframe)[^>]*?>[\s\S]+?<\/\1\s*>/gi, '').replace(/<[^>]+?>/g, '').replace(/\s+/g, ' ').replace(/ /g, ' ').replace(/>/g, ' ');

            // console.log('content: ' + content + ' / text: ' + text);

            if (text.replace(/\ +/g, "").replace(/[\r\n]/g, "").trim() == "") {
                $.fillTipBox({type: 'danger', icon: 'glyphicon-exclamation-sign', content: '评论为空！'});
                // 还原文本
                document.getElementById("submitCommentButton").innerHTML = button_text;
                return false;
            }
            var btn = $("#submitCommentButton");
            btn.attr("disabled", true);
            //tinymce.get('editor').setContent("");

            // 调用可重发的post函数
            postSubmitComment(content, ${resource.id}, button_text)
        } else {
            window.location.href = "<%=request.getContextPath()%>/login";
        }
    }

    function postSubmitComment(content, resourceId, text) {
        $.ajax({
            url: "/resource/newComment",
            type: "post",
            dataType: "text",
            data: {
                content: content,
                resourceId: resourceId
            },
            complete: function() {
                // 还原文本
                document.getElementById("submitCommentButton").innerHTML = text;
            },
            success: function (data) {
                if (data.id == -1) {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '评论失败'});
                    $.tipModal('confirm', 'warning', '发布评论错误，需要重新发布吗?', function (result) {
                        if (result == true) {
                            postSubmitComment(content, resourceId)
                        }
                    });
                } else {
                    $("#content").val("");
                    $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '评论成功'});
                    // 添加评论
                    //addComment(data)
                    getComment(1);
                }
            },
            error: function () {
                $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '评论失败'});
                $.tipModal('confirm', 'warning', '发布评论错误，需要重新发布吗?', function (result) {
                    if (result == true) {
                        postSubmitComment(content, resourceId)
                    }
                });
            },
        });
    }

    function updateComment(commentId) {
        $.tipModal('confirm', 'warning', '确认更新评论吗?', function (result) {
            if (result == true) {
                commentInfo = tinymce.get('comment-editor-box-'+commentId).getContent();
                document.getElementById("comment-content-" + commentId).innerHTML = "评论更新中..."

                // 记录用户名，并修改用户名位置为评论更新中
                userName = document.getElementById("user-name").innerHTML;
                document.getElementById("user-name").innerHTML = "发布更新中...";
                $.ajax({
                    url: "/resource/updateComment",
                    type: "post",
                    async: false,
                    dataType: "text",
                    data: {
                        commentId: commentId,
                        commentInfo: commentInfo,
                    },
                    complete: function() {
                        // 还原用户名
                        document.getElementById("user-name").innerHTML = userName;
                    },
                    success: function (data) {
                        if (data == "ok") {
                            // 更新content内容为新内容，隐藏editor，不刷新页面
                            document.getElementById("comment-content-" + commentId).innerHTML = commentInfo;
                            $("#comment-editor-" + commentId).css('display', 'none');
                            $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '更新成功'});
                        } else {
                            $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '更新失败'});
                        }
                    },
                });
            }
        });
    }

    function deleteComment(commentId) {
        $.tipModal('confirm', 'warning', '确认删除评论吗?', function (result) {
            if (result == true) {
                // 记录用户名，并修改用户名位置为评论删除中
                userName = document.getElementById("user-name").innerHTML;
                document.getElementById("user-name").innerHTML = "发布删除中...";

                $.ajax({
                    url: "/resource/deleteComment",
                    type: "post",
                    async: false,
                    dataType: "text",
                    data: {
                        commentId: commentId,
                    },
                    complete: function() {
                        // 还原用户名
                        document.getElementById("user-name").innerHTML = userName;

                    },
                    success: function (data) {
                        if (data == "ok") {
                            $.fillTipBox({type: 'success', icon: 'glyphicon-ok-sign', content: '删除成功'});

                            // 寻找跳转页（定位上一个评论位置，并展示）
                            // 第一页，怎么删都在第一页
                            if (curPage == 1) {
                                getComment(1);
                            } else {
                                // 如果删除的评论是当前页面的第一个，展示当前页（不可能是第一页）的上一页
                                if(commentIds[0] == commentId) {
                                    getComment(curPage-1);
                                }
                                else {
                                    getComment(curPage);
                                }
                            }
                        } else {
                            $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '删除失败'});
                        }
                    },
                });
            }
        });
    }

    var TimeInterval = 6000;
    var time1 =  setInterval(temp_submit_comment, TimeInterval);
    function temp_submit_comment() {
        var title = $("#title").val();
        // var group = $("#group").val();
        var id = ${resource.id};
        var text = tinyMCE.get("editor").getContent();
        if(text===""){
            return
        }
        // 临时存储评论
        window.localStorage.setItem('comment', JSON.stringify({'userId': ${user_id}, 'resourceId': ${resource.id},
            'commentId': null, 'content': text}));
    }

    //评论自动保存 23.9.9已注释
    <%--function loadTempComment() {--%>

    <%--    let comment = JSON.parse(localStorage.getItem('comment'))--%>
    <%--    if (comment != null && comment['userId'] == ${user_id} && comment['resourceId'] == ${resource.id}--%>
    <%--    && comment['commentId'] == null) {--%>
    <%--        $.tipModal('confirm', 'warning', '有评论发布草稿，需要加载吗?', function (result) {--%>
    <%--            if (result == true) {--%>
    <%--                tinyMCE.activeEditor.setContent(comment['text']);--%>
    <%--                // tinyMCE.get("editor").setContent(comment['text']);--%>
    <%--                window.localStorage.removeItem('comment');--%>
    <%--            } else {--%>
    <%--                window.localStorage.removeItem('comment');--%>
    <%--            }--%>
    <%--        });--%>
    <%--    }--%>
    <%--}--%>


    var time2;
    var edit_comment_id;
    function temp_edit_comment() {
        window.localStorage.setItem('comment', JSON.stringify({'userId': ${user_id}, 'resourceId': ${resource.id},
            'commentId': edit_comment_id, 'content': tinymce.get("comment-content-" + id).getContent()}));
    }

    document.addEventListener("keydown", function (e) {
        if (e.keyCode == 13 && e.ctrlKey) {
            console.log("按下ctrl同时回车")
            $("#submitCommentButton").click();
        }
    });

    document.addEventListener("keydown", function (e) {
        if (e.keyCode == 13 && e.ctrlKey) {
            console.log("按下ctrl同时回车")
            $("#submitCommentButton").click();
        }
    });

    /* 判断是否含有“书籍”标签 */
    $(document).ready(function () {
        let labels = '${resource.labelName}';
        if (labels.search('${bookLabelName}') != -1) {
            $('#operation').append('<button id="btn-enterBook" class="re-btn mt-2 pull-right" onclick="enterBook(${resource.id})">进入书籍</button>');
        }
    });

    /* 进入书籍 */
    function enterBook(id) {
        window.location.href = '/resource/resource?id=' + id + '&isBook=true';
    };


</script>

<style>
    #mytitle {
        position: absolute;
        color: #ffffff;
        max-width: 160px;
        font-size: 14px;
        padding: 4px;
        background: rgba(40, 40, 40, 0.8);
        border: solid 1px #e9f7f6;
        border-radius: 5px;
        z-index: 999;
    }

    .re-btn{
        border-radius: 3px;
    }
</style>

<!-- END: Scripts -->
</html>