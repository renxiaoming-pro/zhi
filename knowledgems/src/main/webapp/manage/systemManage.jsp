<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2021/3/28
  Time: 13:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>系统管理-知了[团队知识管理应用]</title>
</head>

<body>
<div class="re-main">
    <c:import url="../template/_navbar.jsp"/>
    <div class="re-box-6 re-grey mt-85">
        <div class="container">
            <div class="row vertical-gap md-gap">
                <div class="col-lg-12">
                    <div class="re-box-decorated">
                        <div class="re-box-content">
                            <div class="re-form-group mnt-5 mb-9">
                                <a href="../manage">管理</a>&nbsp;|&nbsp;系统管理
                            </div>
                            <h2 class="h4 mnt-5 mb-9">系统管理</h2>
                            <div class="row" style="margin-top: 15px">
                                <div style="margin-left: 21px; width: 68%">
                                    <ul class="re-btn-manage re-btn-manage-style">
                                        <li style="margin: 10px -1px 0">
                                            <a href="#" style="height: 39px; border-radius: 0;"
                                               id="tab-es" class="re-btn-manage-cont" onclick="tabTo('es')">Elasticsearch</a>
                                        </li>
<%--                                        <li style="margin: 10px -1px 0">--%>
<%--                                            <a href="#" style="height: 39px; background-color: white; border-radius: 0;"--%>
<%--                                               id="tab-tika" class="re-btn-manage-cont" onclick="tabTo('tika')">Tika管理</a>--%>
<%--                                        </li>--%>
                                        <li style="margin: 10px -1px 0">
                                            <a href="#" style="height: 39px; background-color: white; border-radius: 0;"
                                               id="tab-nfs" class="re-btn-manage-cont" onclick="tabTo('nfs')">NFS</a>
                                        </li>
                                        <li style="margin: 10px -1px 0">
                                            <a href="#" style="height: 39px; background-color: white; border-radius: 0;"
                                               id="tab-mysql" class="re-btn-manage-cont" onclick="tabTo('mysql')">MySQL</a>
                                        </li>
                                        <li style="margin: 10px -1px 0">
                                            <a href="#" style="height: 39px; background-color: white; border-radius: 0;"
                                               id="tab-book" class="re-btn-manage-cont" onclick="tabTo('book')">应用配置</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="re-separator"></div>
                        <div class="re-form" id="manage-form">
                            <!-- START: ES Config Form -->
                            <div class="re-box-content" style="display: block" id="config-es">
                                <div class="re-form-group row">
                                    <div class="col-md-3 text-right pr-0">Elasticsearch服务地址：</div>
                                    <div class="col-md-9">
                                        <div id="es-addr">${es_addr}</div>
                                    </div>
                                </div>
                                <div class="re-form-group row mt-2">
                                    <div class="col-md-3 text-right pr-0">ES索引更新周期：</div>
                                    <div class="col-md-9">
                                        <div id="es-period">${es_period}&nbsp;s</div>
                                    </div>
                                </div>
                                <div class="re-form-group row mt-2">
                                    <div class="col-md-3 text-right pr-0">ES运行状态：</div>
                                    <div class="col-md-9">
                                        <div id="es-status" style="display: inline;"></div>
                                        <button class="re-btn re-btn-sm btn-primary" onclick="checkEsStatus(true)">再次检测</button>
                                    </div>
                                </div>
                            </div>
                            <!-- END: ES Config Form -->

                            <!-- START: Tika Config Form -->
<%--                            <div class="re-box-content" style="display: none" id="config-tika">--%>
<%--                                <div class="pull-right re-form-group">--%>
<%--                                    <button class="btn btn-primary" onclick="showEdit('tika')">修改</button>--%>
<%--                                </div>--%>
<%--                                <div class="re-form-group">--%>
<%--                                    <label for="tika-addr" class="mnt-7">Tika地址</label>--%>
<%--                                    <input type="text" class="form-control form-control-style-2 col-md-4" id="tika-addr"--%>
<%--                                           placeholder="请输入Tika地址"--%>
<%--                                           value="${tika_addr}" style="background-color: white" disabled="disabled">--%>
<%--                                </div>--%>
<%--                                <div class="re-form-group">--%>
<%--                                    <label for="tika-period" class="mnt-7">“未解析附件”检测周期（单位:秒）--%>
<%--                                        <button class="re-btn re-btn-sm btn-primary" style="margin-left: 20px"--%>
<%--                                                onclick="tikaDetection()">立即检测--%>
<%--                                        </button>--%>
<%--                                    </label>--%>
<%--                                    <input type="number" class="form-control form-control-style-2 col-md-4"--%>
<%--                                           id="tika-period" placeholder="请输入“未解析附件”检测周期"--%>
<%--                                           value="${tika_period}" style="background-color: white" disabled="disabled">--%>
<%--                                </div>--%>
<%--                                <div class="re-form-group">--%>
<%--                                    <label for="tika-retry" class="mnt-7">解析失败重试次数</label>--%>
<%--                                    <input type="number" class="form-control form-control-style-2 col-md-4"--%>
<%--                                           id="tika-retry" placeholder="请输入解析失败重试次数"--%>
<%--                                           value="${tika_retry}" style="background-color: white" disabled="disabled">--%>
<%--                                </div>--%>
<%--                            </div>--%>
                            <!-- END: Tika Config Form -->

                            <!-- START: NFS Config Form -->
                            <div class="re-box-content" style="display: none" id="config-nfs">
                                <div class="re-form-group row">
                                    <div class="col-md-3 text-right pr-0">NFS服务地址：</div>
                                    <div class="col-md-9">
                                        <div id="nfs-addr">${nfs_addr}</div>
                                    </div>
                                </div>
                            </div>
                            <!-- END: NFS Config Form -->

                            <!-- START: MySQL Config Form -->
                            <div class="re-box-content" style="display: none" id="config-mysql">
                                <div class="re-form-group row">
                                    <div class="col-md-3 text-right pr-0">MySQL地址：</div>
                                    <div class="col-md-9">
                                        <div id="mysql-addr">${mysql_addr}</div>
                                    </div>
                                </div>
                                <div class="re-form-group row mt-2">
                                    <div class="col-md-3 text-right pr-0">ES运行状态：</div>
                                    <div class="col-md-9">
                                        <div id="mysql-status" style="display: inline;"></div>
                                        <button class="re-btn re-btn-sm btn-primary" onclick="checkMysqlStatus(true)">再次检测</button>
                                    </div>
                                </div>
                            </div>
                            <!-- END: MySQL Config Form -->

                            <!-- START: BOOK Config Form -->
                            <div class="re-box-content" style="display: none" id="config-book">
                                <div class="re-form-group row">
                                    <div class="col-md-3 text-right pr-0">应用版本：</div>
                                    <div class="col-md-2">
                                        <div id="kms-version" style="display: inline;"></div>
                                    </div>
                                    <button class="re-btn re-btn-sm btn-primary" onclick="checkKmsVersion(true)">刷新</button>
                                </div>
                                <div class="re-form-group row mt-2" id="book-label-area">
                                    <div class="col-md-3 text-right pr-0">“书籍”功能绑定标签ID：</div>
                                    <div class="col-md-2">
                                        <div id="book-label" style="display: inline;">${book_label}</div>
                                    </div>
                                    <button class="re-btn re-btn-sm btn-primary" onclick="editBookLabel()">编辑</button>
                                </div>
                                <div class="re-form-group row mt-2" id="openapi-appkey-area">
                                    <div class="col-md-3 text-right pr-0">openApi-公开appKey：</div>
                                    <div class="col-md-2">
                                        <div id="openapi-appkey" style="display: inline;">${appKey_public}</div>
                                    </div>
                                    <button class="re-btn re-btn-sm btn-primary" onclick="editAppKeyPublic()">编辑</button>
                                </div>
                                <div class="re-form-group row mt-2" id="default-password-area">
                                    <div class="col-md-3 text-right pr-0">用户默认密码：</div>
                                    <div class="col-md-2">
                                        <div id="default-password" style="display: inline;">${default_password}</div>
                                    </div>
                                    <button class="re-btn re-btn-sm btn-primary" onclick="editDefaultPassword()">编辑</button>
                                </div>
                            </div>
                            <!-- END: BOOK Config Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START: Footer -->
    <c:import url="../template/_footer.jsp"/>
    <!-- END: Footer -->
</div>
</body>
<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>
<script type="text/javascript">
    // init
    $(document).ready(function () {
        checkEsStatus(false);
        checkMysqlStatus(false);
        checkKmsVersion(false);
    });

    // switch page
    function tabTo(dest) {
        // tab bar
        $("a.re-btn-manage-cont").each(function () {
           $(this).css("background", "white");
        });
        $("#tab-" + dest).css("background", "");
        // form page
        $("#manage-form > div").each(function () {
            $(this).css("display", "none");
        });
        $("#config-" + dest).css("display", "block");

    }

    // es status
    function checkEsStatus(showTipBox) {
        $.ajax({
            type: "get",
            url: "/manage/esDelay?query=",
            success: function (data) {
                let str = '正常（' + data['time_delay'] + '）';
                $('#es-status').html(str);
                if (showTipBox) {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '检测成功！'});
                }
            },
            error: function () {
                $('#es-status').html('异常');
                if (showTipBox) {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '检测失败！'});
                }
            }
        })
    }

    // mysql status
    function checkMysqlStatus(showTipBox) {
        $.ajax({
            type: "get",
            url: "/manage/mysqlStatus",
            success: function (data) {
                let str = '正常（' + data['time_delay'] + '）';
                $('#mysql-status').html(str);
                if (showTipBox) {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '检测成功！'});
                }
            },
            error: function () {
                $('#mysql-status').html('异常');
                if (showTipBox) {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '检测失败！'});
                }
            }
        })
    }

    // kms version
    function checkKmsVersion(showTipBox) {
        $.ajax({
            type: "get",
            url: "/manage/mysqlStatus",
            success: function (data) {
                $('#kms-version').html(data['kms_version']);
                if (showTipBox) {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '刷新成功！'});
                }
            },
            error: function () {
                $('#kms-version').html('-');
                if (showTipBox) {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '刷新失败！'});
                }
            }
        })
    }

    // edit book label id
    function editBookLabel() {
        let nowId = $('#book-label').html();
        let htmlStr = "";
        htmlStr += "<div class=\"col-md-3 text-right pr-0\">“书籍”功能绑定标签ID：</div>";
        htmlStr += "<div class=\"col-md-2\">";
        htmlStr += "<input id=\"book-label-input\" class=\"form-control form-control-2\"></div>";
        htmlStr += "<button class=\"re-btn re-btn-sm\" onclick=\"submitBookLabel()\" style=\"background-color: #28a745\">保存</button>";
        $('#book-label-area').html(htmlStr);
        $('#book-label-input').val(nowId);
        $('#book-label-input').focus();
    }
    function submitBookLabel() {
        let book_label = $("#book-label-input").val();
        $.ajax({
            type: "post",
            url: "/manage/setBookConfig",
            data: {
                id: book_label
            },
            success: function (data) {
                if (data["code"] == 200) {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '修改成功！'});
                    let htmlStr = '';
                    htmlStr += "<div class=\"col-md-3 text-right pr-0\">“书籍”功能绑定标签ID：</div>";
                    htmlStr += "<div class=\"col-md-2\">";
                    htmlStr += "<div id=\"book-label\" style=\"display: inline;\">" + book_label + "</div>";
                    htmlStr += "</div>";
                    htmlStr += "<button class=\"re-btn re-btn-sm btn-primary\" onclick=\"editBookLabel()\">编辑</button>";
                    $('#book-label-area').html(htmlStr);
                    $('#book-label').html(book_label);
                } else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '修改失败！' + data["msg"]});
                }
            },
            error: function () {
                $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '修改失败！'});
            }
        })
    }

    // edit openapi public appkey
    function editAppKeyPublic() {
        let nowId = $('#openapi-appkey').html();
        let htmlStr = "";
        htmlStr += "<div class=\"col-md-3 text-right pr-0\">openApi-公开appKey：</div>";
        htmlStr += "<div class=\"col-md-2\">";
        htmlStr += "<input id=\"openapi-appkey-input\" class=\"form-control form-control-2\"></div>";
        htmlStr += "<button class=\"re-btn re-btn-sm\" onclick=\"submitAppKeyPublic()\" style=\"background-color: #28a745\">保存</button>";
        $('#openapi-appkey-area').html(htmlStr);
        $('#openapi-appkey-input').val(nowId);
        $('#openapi-appkey-input').focus();
    }
    function submitAppKeyPublic() {
        let appKey_public = $("#openapi-appkey-input").val();
        $.ajax({
            type: "post",
            url: "/manage/setAppKeyPublic",
            data: {
                appKey: appKey_public
            },
            success: function (data) {
                if (data["code"] == 200) {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '修改成功！'});
                    let htmlStr = '';
                    htmlStr += "<div class=\"col-md-3 text-right pr-0\">openApi-公开appKey：</div>";
                    htmlStr += "<div class=\"col-md-2\">";
                    htmlStr += "<div id=\"openapi-appkey\" style=\"display: inline;\">" + appKey_public + "</div>";
                    htmlStr += "</div>";
                    htmlStr += "<button class=\"re-btn re-btn-sm btn-primary\" onclick=\"editAppKeyPublic()\">编辑</button>";
                    $('#openapi-appkey-area').html(htmlStr);
                    $('#openapi-appkey').html(appKey_public);
                } else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '修改失败！' + data["msg"]});
                }
            },
            error: function () {
                $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '修改失败！'});
            }
        })
    }

    // edit default password
    function editDefaultPassword() {
        let default_password = $('#default-password').html();
        let htmlStr = "";
        htmlStr += "<div class=\"col-md-3 text-right pr-0\">用户默认密码：</div>";
        htmlStr += "<div class=\"col-md-2\">";
        htmlStr += "<input id=\"default-password-input\" class=\"form-control form-control-2\"></div>";
        htmlStr += "<button class=\"re-btn re-btn-sm\" onclick=\"submitDefaultPassword()\" style=\"background-color: #28a745\">保存</button>";
        $('#default-password-area').html(htmlStr);
        $('#default-password-input').val(default_password);
        $('#default-password-input').focus();
    }
    function submitDefaultPassword() {
        let default_password = $("#default-password-input").val();
        $.ajax({
            type: "post",
            url: "/manage/setDefaultPassword",
            data: {
                password: default_password
            },
            success: function (data) {
                if (data["code"] == 200) {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '修改成功！'});
                    let htmlStr = '';
                    htmlStr += "<div class=\"col-md-3 text-right pr-0\">用户默认密码：</div>";
                    htmlStr += "<div class=\"col-md-2\">";
                    htmlStr += "<div id=\"default-password\" style=\"display: inline;\">" + default_password + "</div>";
                    htmlStr += "</div>";
                    htmlStr += "<button class=\"re-btn re-btn-sm btn-primary\" onclick=\"editDefaultPassword()\">编辑</button>";
                    $('#default-password-area').html(htmlStr);
                    $('#default-password').html(default_password);
                } else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '修改失败！' + data["msg"]});
                }
            },
            error: function () {
                $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '修改失败！'});
            }
        })
    }

    // show edit modal
    /* function showEdit(name) {
        $("#" + name + "-edit-modal").modal();
    } */

    // submit form
    /* function submitEsConfig() {
        let es_addr = $("#es-addr-edit").val();
        let es_edit = $("#es-period-edit").val();
        $.ajax({
            type: "post",
            url: "/manage/setEsConfig",
            data: {
                addr: es_addr,
                period: es_edit
            },
            success: function (data) {
                if (data["code"] == 200) {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '提交成功！'});
                    $("#es-addr").val(es_addr);
                    $("#es-period").val(es_edit);
                    $("#es-edit-modal").modal("hide");
                } else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '失败！' + data["msg"]});
                }
            },
            error: function () {
                $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '提交异常！'});
            }
        })
    } */

    /* function submitTikaConfig() {
        let tika_addr = $("#tika-addr-edit").val();
        let tika_period = $("#tika-period-edit").val();
        let tika_retry = $("#tika-retry-edit").val();
        $.ajax({
            type: "post",
            url: "/manage/setTikaConfig",
            data: {
                addr: tika_addr,
                period: tika_period,
                retry: tika_retry
            },
            success: function (data) {
                if (data["code"] == 200) {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '提交成功！'});
                    $("#tika-addr").val(tika_addr);
                    $("#tika-period").val(tika_period);
                    $("#tika-retry").val(tika_retry);
                    $("#tika-edit-modal").modal("hide");
                } else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '失败！' + data["msg"]});
                }
            },
            error: function () {
                $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '提交异常！'});
            }
        })
    } */

    /* function submitNfsConfig() {
        let nfs_addr = $("#nfs-addr-edit").val();
        $.ajax({
            type: "post",
            url: "/manage/setNfsConfig",
            data: {
                addr: nfs_addr
            },
            success: function (data) {
                if (data["code"] == 200) {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '提交成功！'});
                    $("#nfs-addr").val(nfs_addr);
                    $("#nfs-edit-modal").modal("hide");
                } else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '失败！' + data["msg"]});
                }
            },
            error: function () {
                $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '提交异常！'});
            }
        })
    } */

    /* function submitBookConfig () {
        let book_label = $("#book-label-edit").val();
        $.ajax({
            type: "post",
            url: "/manage/setBookConfig",
            data: {
                id: book_label
            },
            success: function (data) {
                if (data["code"] == 200) {
                    $.fillTipBox({type: 'success', icon: 'glyphicon-alert', content: '提交成功！'});
                    $("#book-label").val(book_label);
                    $("#book-edit-modal").modal("hide");
                } else {
                    $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '失败！' + data["msg"]});
                }
            },
            error: function () {
                $.fillTipBox({type: 'danger', icon: 'glyphicon-alert', content: '提交异常！'});
            }
        })
    } */

</script>
<style>
    .re-btn{
        border-radius: 3px;
    }
</style>
<!-- END: Scripts -->
</html>

