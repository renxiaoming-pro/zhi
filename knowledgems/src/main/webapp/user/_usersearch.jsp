<%--
  Created by IntelliJ IDEA.
  User: sky
  Date: 2023/9/11
  Time: 17:31
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>usersearch</title>
    <style>
        @media (max-width: 767px) {
            .hideTel{
                display: none;
                width: 500px;
            }
        }
    </style>
</head>
<body>
<div class="row" style="margin-top: 15px">
    <div style="margin-left: 21px;margin-right: auto; width: 55%;">
        <ul class="re-btn-manage re-btn-manage-style">
            <li style="margin: 10px -1px 0">
                <a href="#" style="height: 39px; border-radius: 0" class="re-btn-manage-cont" onclick="showUsers(1,'')">用户</a>
            </li>
            <li style="margin: 10px -1px 0">
                <a href="#" style="height: 39px; border-radius: 0" class="re-btn-manage-cont" onclick="showManager(1,'')">管理员</a>
            </li>
            <li style="margin: 10px -1px 0">
                <a href="#" style="height: 39px; border-radius: 0" class="re-btn-manage-cont" onclick="showApply(1,'')">用户注册请求 ( ${applyNum} )</a>
            </li>
            <li style="margin: 10px -1px 0">
                <a href="#" style="height: 39px; background-color: white; border-radius: 0" class="re-btn-manage-cont">搜索到用户 ( ${searchedUserNum} )</a>
            </li>
        </ul>
    </div>
</div>
<div id="dataList" class="panel panel-default front-panel" style="margin-top:10px">
    <table id="infoTable" class="table table-striped front-table table-bordered" style="margin-bottom: 0px">
        <thead>
        <tr>
            <th style="text-align: center">姓名</th>
            <th style="text-align: center">手机</th>
            <th style="text-align: center">邮箱</th>
            <th style="text-align: center">角色</th>
            <th style="text-align: center">操作</th>
        </tr>
        </thead>
        <c:forEach var="user" items="${userList}">
            <tr id="${user.id}">
                <td style="vertical-align: middle;overflow: auto;text-align: center"><a href="/myresource?userId=${user.id}" style="color: black">${user.realName}</a></td>
                <td class="hideTel" style="vertical-align: middle;overflow: auto;text-align: center">${user.account}</td>
                <td class="hideTel" style="vertical-align: middle;overflow: auto;text-align: center">${user.mail}</td>
                <td style="vertical-align: middle;overflow: auto;text-align: center">
                    <c:if test="${user.roleId == 1}">
                        超级管理员
                    </c:if>
                    <c:if test="${user.roleId == 2}">
                        用户管理员
                    </c:if>
                    <c:if test="${user.roleId == 3}">
                        内容管理员
                    </c:if>
                    <c:if test="${user.roleId == 4}">
                        系统管理员
                    </c:if>
                    <c:if test="${user.roleId == 0}">
                        普通用户
                    </c:if>
                    <c:choose>
                        <c:when test="${empty user.appKey}">
                            <div id="${user.id}-appKey">appKey:未激活</div>
                        </c:when>
                        <c:otherwise>
                            <div id="${user.id}-appKey">appKey:${user.appKey}</div>
                        </c:otherwise>
                    </c:choose>
                </td>
                <td style="vertical-align: middle;overflow: auto;text-align: center">
                    <c:if test="${sessionScope.roleId == 1}">
                        <c:if test="${user.roleId == 1}">
                            <c:if test="${user.id == sessionScope.userId}">
                                <div>
                                    <a href="javascript:void(0)" onclick="showUserModify('${user.realName}',${user.account},'${user.mail}',${user.roleId},${user.id},'${sessionScope.roleId}')">编辑</a>
                                    <a href="javascript:void(0)" onclick="confirmDeleteUser(${user.id})">删除</a>
                                </div>
                                <div>
                                    <a href="javascript:void(0)" onclick="confirmAppKey(${user.id})">激活</a>
                                    <a href="javascript:void(0)" onclick="confirmResetPassword(${user.id})">密码</a>
                                </div>
                            </c:if>
                            <c:if test="${user.id != sessionScope.userId}">
                                <div>
                                    <a href="javascript:void(0)" onclick="showWrongTip()">编辑</a>
                                    <a href="javascript:void(0)" onclick="showWrongTip()">删除</a>
                                </div>
                                <div>
                                    <a href="javascript:void(0)" onclick="showWrongTip()">激活</a>
                                    <a href="javascript:void(0)" onclick="showWrongTip()">密码</a>
                                </div>
                            </c:if>
                        </c:if>
                        <c:if test="${user.roleId == 0 || user.roleId == 2 || user.roleId == 3 || user.roleId == 4}">
                            <div>
                                <a href="javascript:void(0)" onclick="showUserModify('${user.realName}',${user.account},'${user.mail}',${user.roleId},${user.id},'${sessionScope.roleId}')">编辑</a>
                                <a href="javascript:void(0)" onclick="confirmDeleteUser(${user.id})">删除</a>
                            </div>
                            <div>
                                <a href="javascript:void(0)" onclick="confirmAppKey(${user.id})">激活</a>
                                <a href="javascript:void(0)" onclick="confirmResetPassword(${user.id})">密码</a>
                            </div>
                        </c:if>
                    </c:if>

                    <c:if test="${sessionScope.roleId == 2}">
                        <c:if test="${user.roleId == 1}">
                            <div>
                                <a href="javascript:void(0)" onclick="showWrongTip()">编辑</a>
                                <a href="javascript:void(0)" onclick="showWrongTip()">删除</a>
                            </div>
                            <div>
                                <a href="javascript:void(0)" onclick="showWrongTip()">激活</a>
                                <a href="javascript:void(0)" onclick="showWrongTip()">密码</a>
                            </div>
                        </c:if>
                        <c:if test="${user.roleId == 0}">
                            <div>
                                <a href="javascript:void(0)" onclick="showUserModify('${user.realName}',${user.account},'${user.mail}',${user.roleId},${user.id},'${sessionScope.roleId}')">编辑</a>
                                <a href="javascript:void(0)" onclick="confirmDeleteUser(${user.id})">删除</a>
                            </div>
                            <div>
                                <a href="javascript:void(0)" onclick="confirmAppKey(${user.id})">激活</a>
                                <a href="javascript:void(0)" onclick="confirmResetPassword(${user.id})">密码</a>
                            </div>
                        </c:if>
                        <c:if test="${user.roleId == 2 || user.roleId == 3 || user.roleId == 4}">
                            <c:if test="${user.id == sessionScope.userId}">
                                <div>
                                    <a href="javascript:void(0)" onclick="showUserModify('${user.realName}',${user.account},'${user.mail}',${user.roleId},${user.id},'${sessionScope.roleId}')">编辑</a>
                                    <a href="javascript:void(0)" onclick="confirmDeleteUser(${user.id})">删除</a>
                                </div>
                                <div>
                                    <a href="javascript:void(0)" onclick="confirmAppKey(${user.id})">激活</a>
                                    <a href="javascript:void(0)" onclick="confirmResetPassword(${user.id})">密码</a>
                                </div>
                            </c:if>
                            <c:if test="${user.id != sessionScope.userId}">
                                <div>
                                    <a href="javascript:void(0)" onclick="showWrongTip()">编辑</a>
                                    <a href="javascript:void(0)" onclick="showWrongTip()">删除</a>
                                </div>
                                <div>
                                    <a href="javascript:void(0)" onclick="confirmAppKey(${user.id})">激活</a>
                                    <a href="javascript:void(0)" onclick="confirmResetPassword(${user.id})">密码</a>
                                </div>
                            </c:if>
                        </c:if>
                    </c:if>

                    <c:if test="${sessionScope.roleId == 3 || sessionScope.roleId == 4}">
                        <c:if test="${user.roleId == 1}">
                            <div>
                                <a href="javascript:void(0)" onclick="showWrongTip()">编辑</a>
                                <a href="javascript:void(0)" onclick="showWrongTip()">删除</a>
                            </div>
                            <div>
                                <a href="javascript:void(0)" onclick="showWrongTip()">激活</a>
                                <a href="javascript:void(0)" onclick="showWrongTip()">密码</a>
                            </div>
                        </c:if>
                        <c:if test="${user.roleId == 0 || user.roleId == 2 || user.roleId == 3 || user.roleId == 4}">
                            <c:if test="${user.id == sessionScope.userId}">
                                <div>
                                    <a href="javascript:void(0)" onclick="showUserModify('${user.realName}',${user.account},'${user.mail}',${user.roleId},${user.id},'${sessionScope.roleId}')">编辑</a>
                                    <a href="javascript:void(0)" onclick="confirmDeleteUser(${user.id})">删除</a>
                                </div>
                                <div>
                                    <a href="javascript:void(0)" onclick="confirmAppKey(${user.id})">激活</a>
                                    <a href="javascript:void(0)" onclick="confirmResetPassword(${user.id})">密码</a>
                                </div>
                            </c:if>
                            <c:if test="${user.id != sessionScope.userId}">
                                <div>
                                    <a href="javascript:void(0)" onclick="showWrongTip()">编辑</a>
                                    <a href="javascript:void(0)" onclick="showWrongTip()">删除</a>
                                </div>
                                <div>
                                    <a href="javascript:void(0)" onclick="confirmAppKey(${user.id})">激活</a>
                                    <a href="javascript:void(0)" onclick="confirmResetPassword(${user.id})">密码</a>
                                </div>
                            </c:if>
                        </c:if>
                    </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<div id="usersearchpageNumbers" class="text-center"></div>
</body>
<script>
    showPageNumbers();
    function showPageNumbers(){
        var htmlstr = "";
        var page = parseInt("${currentPage}");
        var maxPage = parseInt("${maxPages}");
        var maxShowPage = 10;
        if(maxShowPage > maxPage) maxShowPage = maxPage;
        if(maxPage <= 1){
            return;
        }
        htmlstr += "<div class=\"text-center\">" +
            "<ul class=\"re-pagination mt-40\">";
        if(page <= 1){
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a aria-label=\"Previous\"><span class=\"icon pe-7s-angle-left\"></span></a></li>";
        }
        else{
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a href=\"javascript:getAppPage("+ (page-1).toString() + ");\" aria-label=\"Previous\"><span class=\"icon pe-7s-angle-left\"></span></a></li>";
        }
        if(maxPage <= maxShowPage){
            for(i = 1; i <= maxShowPage; i++){
                if(i === page){
                    htmlstr += "<li class=\"active\"><a>"+ i.toString() +"</a></li>";
                }
                else{
                    htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                }
            }
        }
        else{
            if(page < maxShowPage - 2){
                for(i = 1; i <= maxShowPage - 2; i++){
                    if(i === page){
                        htmlstr += "<li class=\"active\"><a>"+ i.toString() +"</a></li>";
                    }
                    else{
                        htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                    }
                }
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+maxPage.toString()+");\" aria-label=\"Last\">"+maxPage.toString()+"</a></li>";
            }
            else if(page >= maxShowPage - 2 && page <= maxPage - maxShowPage + 3){
                htmlstr += "<li><a href=\"javascript:getAppPage(1);\">1</a></li>";
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+ (page-2).toString() +");\">"+ (page-2).toString() +"</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+ (page-1).toString() +");\">"+ (page-1).toString() +"</a></li>";
                htmlstr += "<li class=\"active\"><a>"+ page.toString() +"</a></li>";
                for(i = page + 1; i <= maxShowPage - 7 + page; i++){
                    htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                }
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                htmlstr += "<li><a href=\"javascript:getAppPage("+maxPage.toString()+");\" aria-label=\"Last\">"+maxPage.toString()+"</a></li>";
            }
            else{
                htmlstr += "<li><a href=\"javascript:getAppPage(1);\">1</a></li>";
                htmlstr += "<li><a aria-label=\"AfterMore\">...</a></li>";
                for(i = maxPage - maxShowPage + 3; i <= maxPage; i++){
                    if(i === page){
                        htmlstr += "<li class=\"active\"><a>"+ i.toString() +"</a></li>";
                    }
                    else{
                        htmlstr += "<li><a href=\"javascript:getAppPage("+ i.toString() +");\">"+ i.toString() +"</a></li>";
                    }
                }
            }
        }
        if(parseInt(page) >= maxPage){
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a aria-label=\"Next\"><span class=\"icon pe-7s-angle-right\"></span></a></li>";
        }
        else{
            htmlstr += "<li class=\"re-pagination-icon disabled\"><a href=\"javascript:getAppPage("+ (parseInt(page)+1).toString() + ");\" aria-label=\"Next\"><span class=\"icon pe-7s-angle-right\"></span></a></li>";
        }
        htmlstr += "</ul>" +
            "</div>"
        $("#usersearchpageNumbers").html(htmlstr);
    }
</script>
</html>

