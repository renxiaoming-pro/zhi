<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/9/4
  Time: 15:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>

    <link rel="stylesheet" type="text/css" href="../newcss/css/newCSS.css">

    <title>首页-知了[团队知识管理应用]</title>
</head>

<body onload="focus_init()">
<div class="re-main">
    <c:import url="../template/_navbar.jsp?menu=index"/>
    <header class="re-header re-box-1">
        <div class="container">
            <div class="home-image home-image-parallax">
                <img src="../assets/images/bg-header-2.png" alt="">
                <div style="background-color: rgba(27, 27, 27, .8);"></div>
            </div>

            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                    <h1 class="mb-30 text-white text-center">团队知识管理</h1>
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"></div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <form id="search-form" class="re-form re-form-group-inputs">
                                <%--输入框--%>
                                <input type="text" style="display: none;"/>
                                <input type="hidden" name="keyword" id="hkeyword"/>
                                <input id="query" type="text" name="queryStr" class="form-control re-btn-rd" placeholder="在这里发现你想要的知识"/>
                                <button type="button" class="re-btn re-btn-lg re-btn-r" onclick="searchData(1)"><a>搜&nbsp;&nbsp;索</a></button>
                            </form>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"></div>
                    <%--                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2 col-2"--%>
<%--                             style="display: flex;/*实现垂直居中*/align-items: center;padding-left: 0px;">--%>
<%--                            <a href="../adsearch/adsearch" style="color: #ffffff">高级</a>--%>
<%--                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container re-box-6 re-grey">
        <div class="row vertical-gap">
            <div class="col-12 col-md-6 col-lg-4">
                <div class="re-portfolio-item">
                    <div class="re-portfolio-item-image" style="height: 340px">
                        <img src="../assets/images/index_0001.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="re-portfolio-item">
                    <div class="re-portfolio-item-image" style="height: 340px">
                        <img src="../assets/images/index_0002.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <div class="re-portfolio-item">
                    <div class="re-portfolio-item-image" style="height: 340px">
                        <img src="../assets/images/index_0003.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div >
    <c:import url="../template/_footer.jsp"/>
</div>

</body>
<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>

<script type="text/javascript">
    function focus_init(){
        //alert("warning");
        // 光标居中
        setTimeout(function () {
            $("#query").focus();
        },500)
    }

    $('input').focus();
    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            <%
                session.setAttribute("queryStr", request.getParameter("queryStr"));
                //System.out.println("query: "+request.getParameter("queryStr"));
            %>
            var query = $("#query").val().trim();
            var times = 1;
            window.location.href = "<%=request.getContextPath()%>/search/searchAll?queryStr=" + query + "&groupIds=" + "&labelIds=" + "&times=" + times;
            searchTimes = times;
        }
    });
    function searchData() {
        var times = 1;
        // times = searchTimes+1;
        <%
            session.setAttribute("queryStr", request.getParameter("queryStr"));
        %>
        var query = $("#query").val().trim();
        window.location.href = "<%=request.getContextPath()%>/search/searchAll?queryStr=" + query + "&groupIds=" + "&labelIds=" + "&times=" + times;
    }
</script>
<!-- END: Scripts -->
</html>
